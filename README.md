The project 

It is a live poling/voting system which can be used during workshops, webinars, live events etc. 

How? 
Presenter can beforehand go to the home page of the system and create a survey/vote/question (eventually). Once the vote is created it will be given unique link (selectable by the 
designer). During the workshop the link is communicated to the participants where they see the vote and answer right away. Live voting information is feeded to the vote page (updated live).

